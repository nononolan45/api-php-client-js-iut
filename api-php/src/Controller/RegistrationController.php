<?php

namespace App\Controller;

use App\Form\UserType;
use App\Entity\Ussers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class RegistrationController extends Controller {

    /**
    * @Security("has_role('ROLE_ADMIN')")
    * @SWG\Response(
    *    description="Création d'un utilisateur",
    *    response=201,
    *    @SWG\Schema(
    *            type="json",
    *    )
    * )
  * @Rest\Post(
  *   path ="/api/inscription-user",
  *   name="creation_user"
  * )
  * @Rest\View(StatusCode = 201)
  * @ParamConverter("user", converter="fos_rest.request_body")
  */
    public function userAction(Ussers $user, UserPasswordEncoderInterface $passwordEncoder) {
        $password = $passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($password);
        $user->setIsActive(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

       return $user;
     }
     /**
     * @Security("has_role('ROLE_ADMIN')")
     * @SWG\Response(
     *    description="Création d'un administrateur",
     *    response=201,
     *    @SWG\Schema(
     *            type="json",
     *    )
     * )
   * @Rest\Post(
   *   path ="/api/inscription-admin",
   *   name="creation_admin"
   * )
   * @Rest\View(StatusCode = 201)
   * @ParamConverter("user", converter="fos_rest.request_body")
   */
     public function adminAction(Ussers $user, UserPasswordEncoderInterface $passwordEncoder) {
         $password = $passwordEncoder->encodePassword($user, $user->getPassword());
         $user->setPassword($password);
         $user->setIsActive(true);
         $user->addRole("ROLE_ADMIN");
         $entityManager = $this->getDoctrine()->getManager();
         $entityManager->persist($user);
         $entityManager->flush();

        return $user;
      }
      /**
      * @SWG\Response(
      *    description="Connexion utilisateur",
      *    response=201
      *
      * )
    * @Rest\Post(
    *   path ="/login",
    *   name="connexion"
    * )
    * @Rest\View(StatusCode = 201)
    */
    public function login() {
    return new JsonResponse(['message' => 'Connexio OK']);
      
    }


}
