<?php
namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

class IndexController extends Controller
{
/**
* @SWG\Response(
*    description="Page principale",
*    response=200
* )
*
* @Rest\Get(
*      path="/",
*      name="index"
* )
 */
    public function index()
    {
        return $this->render('index.html.twig');
    }

}
