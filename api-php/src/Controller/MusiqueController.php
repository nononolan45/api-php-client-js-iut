<?php

namespace App\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\ApiMusique;
use App\Repository\ApiMusiqueRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swagger\Annotations as SWG;

class MusiqueController extends Controller
{

    /**
    * @SWG\Response(
    *    description="Listes des musiques",
    *    response=201,
    *    @SWG\Schema(
    *            type="json",
    *    )
    * )
    *
    * @Rest\Get(
    *      path="/musiques",
    *      name="list_musique"
    * )
    *
    * @Rest\View(
    *      statusCode = 201
    * )
    */
    public function getAllMusiques()
    {
        $musiques = $this->getDoctrine()
          ->getRepository('App:ApiMusique')
          ->findAll();
            return $musiques;
}

/**
* @SWG\Response(
*    description="Musique identifiée par son id",
*    response=201,
*    @SWG\Schema(
*            type="json",
*    )
* )
* @SWG\Parameter(
*     name="id",
*     in="path",
*     type="integer",
*     description="Identifiant unique  d'une musique"
* )
 * @Rest\Get(
 *      path = "/musiques/{id}",
 *      name = "une_musique",
 *      requirements = {"id"="\d+"}
 * )
 * @Rest\View(
 *      statusCode = 201
 * )
 */
public function getMusique(int $id)
{
  $musique = $this->getDoctrine()
    ->getRepository('App:ApiMusique')
    ->findById($id);
    if(empty($musique)){
      return new JsonResponse(['message' => 'Musique inexistante'], Response::HTTP_NOT_FOUND);
    }
    else {



    return $musique;
  }
}



/**
* @SWG\Response(
*    description="Musique identifiée par son genre ",
*    response=201,
*    @SWG\Schema(
*            type="json",
*    )
* )
* @SWG\Parameter(
*     name="genre",
*     in="path",
*     type="string",
*     description="Genre d'une musique"
* )
 * @Rest\Get(
 *      path = "/musiques/genre/{genre}",
 *      name = "un_genre"
 * )
 * @Rest\View(
 *      statusCode = 201
 * )
 */
public function getGenre(string $genre)
{
  $musique = $this->getDoctrine()
    ->getRepository('App:ApiMusique')
    ->findBy(['genre' => $genre]);
    if(empty($musique)){
      return new JsonResponse(['message' => 'Genre inexistant'], Response::HTTP_NOT_FOUND);
    }
    else {



    return $musique;
  }
}



/**
* @SWG\Response(
*    description="Musique identifiée par son auteur ",
*    response=201,
*    @SWG\Schema(
*            type="json",
*    )
* )
* @SWG\Parameter(
*     name="auteur",
*     in="path",
*     type="string",
*     description="Auteur d'une musique"
* )
 * @Rest\Get(
 *      path = "/musiques/auteur/{auteur}",
 *      name = "un_auteur"
 * )
 * @Rest\View(
 *      statusCode = 201
 * )
 */
public function getAuteur(string $auteur)
{
  $musique = $this->getDoctrine()
    ->getRepository('App:ApiMusique')
    ->findBy(['auteur' => $auteur]);
    if(empty($musique)){
      return new JsonResponse(['message' => 'Auteur inexistant'], Response::HTTP_NOT_FOUND);
    }
    else {

    return $musique;
  }
}


/**
* @SWG\Response(
*    description="Musique identifiée par son titre ",
*    response=201,
*    @SWG\Schema(
*            type="json",
*    )
* )
* @SWG\Parameter(
*     name="titre",
*     in="path",
*     type="string",
*     description="Titre d'une musique"
* )
 * @Rest\Get(
 *      path = "/musiques/titre/{titre}",
 *      name = "un_titre"
 * )
 * @Rest\View(
 *      statusCode = 201
 * )
 */
public function getTitre(string $titre)
{
  $musique = $this->getDoctrine()
    ->getRepository('App:ApiMusique')
    ->findBy(['titre' => $titre]);
    if(empty($musique)){
      return new JsonResponse(['message' => 'Titre inexistant'], Response::HTTP_NOT_FOUND);
    }
    else {



    return $musique;
  }
}





/**
* @SWG\Response(
*    description="Musique identifiée par son année ",
*    response=201,
*    @SWG\Schema(
*            type="json",
*    )
* )
* @SWG\Parameter(
*     name="annee",
*     in="path",
*     type="integer",
*     description="Année d'une musique"
* )
 * @Rest\Get(
 *      path = "/musiques/annee/{annee}",
 *      name = "une_annee"
 * )
 * @Rest\View(
 *      statusCode = 201
 * )
 */
public function getAnnee(int $annee)
{
  $musique = $this->getDoctrine()
    ->getRepository('App:ApiMusique')
    ->findBy(['annee' => $annee]);
    if(empty($musique)){
      return new JsonResponse(['message' => 'Musique inexistante'], Response::HTTP_NOT_FOUND);
    }
    else {



    return $musique;
  }
}




/**
* @SWG\Response(
*    description="Musique identifiée par son genre, année, titre, et auteur",
*    response=201,
*    @SWG\Schema(
*            type="json",
*    )
* )
* @SWG\Parameter(
*     name="genre",
*     in="path",
*     type="string",
*     description="Genre d'une musique"
* )
* @SWG\Parameter(
*     name="titre",
*     in="path",
*     type="string",
*     description="Titre d'une musique"
* )
* @SWG\Parameter(
*     name="auteur",
*     in="path",
*     type="string",
*     description="Auteur d'une musique"
* )
* @SWG\Parameter(
*     name="annee",
*     in="path",
*     type="integer",
*     description="Annee d'une musique"
* )
 * @Rest\Get(
 *      path = "/musiques/{auteur}/{genre}/{annee}/{titre}",
 *      defaults={"auteur"="","genre"="","annee"="","titre"=""},
 *      name = "multiple"
 * )
 * @Rest\View(
 *      statusCode = 201
 * )
 */
public function getMultiple(string $auteur, string $genre, int $annee, string $titre)
{
  $musique = $this->getDoctrine()
    ->getRepository('App:ApiMusique')
    ->findBy(['auteur' => $auteur,
              'genre' => $genre,
              'annee' => $annee,
              'titre' => $titre]);
    if(empty($musique)){
      return new JsonResponse(['message' => 'Musique inexistante'], Response::HTTP_NOT_FOUND);
    }
    else {



    return $musique;
  }
}




/**
* @SWG\Response(
*    description=" Musique mise à jour par le biais de l'indentifiant unique",
*    response=200,
*    @SWG\Schema(
*            type="json",
*    )
* )
* @SWG\Parameter(
*     name="id",
*     in="path",
*     type="integer",
*     description="Identifiant d'une musique"
* )
 * @Rest\Put(
 *      path = "/api/musiques/{id}",
 *      requirements = {"id"="\d+"}
 * )
 * @Rest\View(statusCode = 200)
 */
public function putMusique(int $id, Request $request)
{
  $musique = $this->getDoctrine()
    ->getRepository('App:ApiMusique')
    ->findById($id);
    var_dump($musique);
    if(empty($musique)){
      return new JsonResponse(['message' => 'Musique inexistante'], Response::HTTP_NOT_FOUND);
    }
    else {
    $musique[0]->setAuteur($request->get('auteur'));
    $musique[0]->setGenre($request->get('genre'));
    $musique[0]->setImage($request->get('image'));
    $musique[0]->setAnnee($request->get('annee'));
    $musique[0]->setTitre($request->get('titre'));
    $em = $this->getDoctrine()->getManager();
    $em->persist($musique[0]);
    $em->flush();
    return $musique;
  }}





  /**
  * @Security("has_role('ROLE_USER')")
  * @SWG\Response(
  *    description="Création d'une musique",
  *    response=201,
  *    @SWG\Schema(
  *            type="json",
  *    )
  * )
* @Rest\Post(
*   path ="/api/musiques",
*   name="creation_musique"
* )
* @Rest\View(StatusCode = 201)
* @ParamConverter("musique", converter="fos_rest.request_body")
*/
public function postCreateMusique(ApiMusique $musique)
{
  $em = $this->getDoctrine()->getManager();
  $em->persist($musique);
  $em->flush();

   return $musique;

}

/**
* @Security("has_role('ROLE_USER')")
* @SWG\Response(
*    description=" Supression d'une musique  par le biais de l'indentifiant unique",
*    response=200,
*    @SWG\Schema(
*            type="json",
*    )
* )
* @SWG\Parameter(
*     name="id",
*     in="path",
*     type="integer",
*     description="Identifiant d'une musique"
* )
 * @Rest\Delete(
 *      path ="/api/musiques/{id}",
 *      name="supression_musique"
 * )
 * @Rest\View(StatusCode = 204)
 */
public function deleteArticle(int $id)
{

    $musique = $this->getDoctrine()
      ->getRepository('App:ApiMusique')
      ->findById($id);
      if(empty($musique)){
        return new JsonResponse(['message' => 'Musique inexistante'], Response::HTTP_NOT_FOUND);
      }
      else {
      $em = $this->getDoctrine()->getManager();
      $em->remove($musique[0]);
      $em->flush();



      return new JsonResponse(['message' => 'Musique supprimé']);

      }
}

}
