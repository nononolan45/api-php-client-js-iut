GROUPE PIRES,BARBET,PARDE



Le client JS a été developpé de facon à etre totalemnt en SPA ;
L'api PHP REST propose divers routes qui permettent :
-l'affichage de toute une liste de musiques
-la modification d'une musique identifié par son id
-la suppression d'une musique identifié par son id
-l'ajout d'une musique
-l'affichage d'une liste de musique suivant la recherche d'un critère précis
-l'ajout d'un utilisateur et d'un administrateur
-la connexion d'un utilisateur et/ou administateur
-l'affichage de la documenation de l'api
Pour plus de détails technique sur les routes de l'api , on peux se rendre à l'adresse suivante http://localhost:8000/doc
Les anonymes (personne non connectée) ne peux que avoir accées à l'affichage d'une lsite de musique ou de sa totalité.
Un utilisateur possède les mêmes droits qu'une personne anonyme. Il peux en plus modifier, supprimer ou ajouter une musique à la base de données.
Un administateur possède la totalité des droits. Il peux de plus créer des utilisateurs et administateurs.
Le client js, propose une seule page web qui se met à jour à chaque action. Lors du chargement de celle ci, nous sommes defini comme une personne anonyme ,
donc elle charge autotmatiquement la liste de musique.
Un formulaire de connexion est proposé, et vous recevez une notification lors de la validation de celui ci. Il va permetter d'ajouter une musique grace au bouton dédié
qui charge un formulaire.
Lors d'un click sur une musique en particulier, deux boutons vont apparaitre. L'un pour supprimer la musique, l'autre pour la modifier.
De plus il y a champs de recherche pour trier la musique suivant ces critères (auteur, genre, annee, titre)