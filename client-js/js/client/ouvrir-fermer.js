function ffermer(){
	document.getElementById('container1').style.display = "none";
	document.getElementById('fondformu').style.display = "none";
}

function ouvrir(){
	document.getElementById('container1').style.display = "block";
	document.getElementById('form').style.display = "block";
	document.getElementById('fondformu').style.display = "grid";
	document.getElementById('form2').style.display = "none";
}

function ffermerModifier(){
	document.getElementById('container1').style.display = "none";
	document.getElementById('fondformu').style.display = "none";
}

function ouvrirModifier(id){
	document.getElementById('container1').style.display = "block";
	document.getElementById('form2').style.display = "block";
	document.getElementById('fondformu').style.display = "grid";
	document.getElementById('form').style.display = "none";

	fetch("http://localhost:8000/musiques/"+id, {
        method: 'GET'
    }).then(response => response.json())
    .then(data => {
    console.log(data);
    document.getElementById("formAnnee2").value = data[0].annee;
		document.getElementById("formGenre2").value = data[0].genre;
		document.getElementById("formAuteur2").value = data[0].auteur;
		document.getElementById("formTitre2").value = data[0].titre;
		document.getElementById("formImage2").value = data[0].image;
		document.getElementById("formId2").value = data[0].id;
    })
    .catch(error => console.error(error));

}
