let auth;
const myHeaders = new Headers();

myHeaders.append('Content-Type', 'application/json');
// fonction checker l'état du token
function checkAuth(){
  console.log(auth);
}
//focntion charger les données d'un fichier .json
function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}
//fonction pour recuperer le json de toutes les musiques , et en faire une liste
function ajaxGet(){
    fetch("http://localhost:8000/musiques", {
        method: 'GET',
        headers: new Headers({
            'Content-Type': 'application/json',
        })
    }).then(response => response.json())
    .then(data => {
        console.log(data);
        data.forEach(function (musique) {

            let elem0 = document.createElement('a');
            elem0.id = "link"+musique.id;
            elem0.setAttribute('href','#');
            elem0.style.cssText = "text-decoration : none";
            elem0.style.cssText = "border-radius: 100px;";

            let elem1 = document.createElement('div');
            elem1.className = "divMusique";
            elem1.id = musique.id;
            elem1.setAttribute('onclick','createSelect(this.id);');
            elem1.style.cssText = "border-bottom-right-radius: 13px 13px;";

            let elem2 = document.createElement('div');
            elem2.className = "divId";
            elem2.innerHTML = "<strong><i>musique n° " + musique.id+" </i>";

            let elem3 = document.createElement('img');
            elem3.className = "divImage";
            elem3.setAttribute('src','server/php/files/'+musique.image);
            elem3.style.cssText = "background-color:#b9a887; height:65px; border-top-right-radius: 13px 13px; border-bottom-right-radius: 13px 13px;";

            let elem4 = document.createElement('div');
            elem4.className = "divTitre";
            elem4.innerHTML = "<strong><i>titre:</i></strong> " + "</br>" + musique.titre;

            let elem5 = document.createElement('div');
            elem5.className = "divAuteur";
            elem5.innerHTML = "<strong><i>auteur:</i></strong> " + "</br>" + musique.auteur;

            let elem6 = document.createElement('div');
            elem6.className = "divGenre";
            elem6.innerHTML = "<strong><i>genre:</i></strong> " + "</br>" + musique.genre;

            let elem7 = document.createElement('div');
            elem7.className = "divAnnee";
            elem7.innerHTML = "<strong><i>année:</i></strong> " + "</br>" + musique.annee;

            document.getElementById('musiques').appendChild(elem0);
            document.getElementById('link'+musique.id).appendChild(elem1);
            document.getElementById(musique.id).appendChild(elem2);
            document.getElementById(musique.id).appendChild(elem3);
            document.getElementById(musique.id).appendChild(elem4);
            document.getElementById(musique.id).appendChild(elem5);
            document.getElementById(musique.id).appendChild(elem6);
            document.getElementById(musique.id).appendChild(elem7);

        });
    })
    .catch(error => console.error(error));
}
//fonction pour recuperer le json par auteur defini, et en faire une liste
function ajaxAuteur(){

    let filtre = document.getElementById('filtre').value;
    fetch("http://localhost:8000/musiques/auteur/"+filtre, {
        method: 'GET',
        headers: new Headers({
            'Content-Type': 'application/json',
        })
    }).then(response => response.json())
    .then(data => {
        console.log(data);
        console.log(filtre);
        data.forEach(function (musique) {

            let elem0 = document.createElement('a');
            elem0.id = "link"+musique.id;
            elem0.setAttribute('href','#')

            let elem1 = document.createElement('div');
            elem1.className = "divMusique";
            elem1.id = musique.id;
            elem1.setAttribute('onclick','createSelect(this.id);')

            let elem2 = document.createElement('div');
            elem2.className = "divId";
            elem2.innerHTML = "musique n° : " + musique.id;

            let elem3 = document.createElement('img');
            elem3.className = "divImage";
            elem3.setAttribute('src','server/php/files/'+musique.image);

            let elem4 = document.createElement('div');
            elem4.className = "divTitre";
            elem4.innerHTML = "titre : " + musique.titre;

            let elem5 = document.createElement('div');
            elem5.className = "divAuteur";
            elem5.innerHTML = "auteur : " + musique.auteur;

            let elem6 = document.createElement('div');
            elem6.className = "divGenre";
            elem6.innerHTML = "genre : " + musique.genre;

            let elem7 = document.createElement('div');
            elem7.className = "divAnnee";
            elem7.innerHTML = "année : " + musique.annee;

            document.getElementById('musiques').appendChild(elem0);
            document.getElementById('link'+musique.id).appendChild(elem1);
            document.getElementById(musique.id).appendChild(elem2);
            document.getElementById(musique.id).appendChild(elem3);
            document.getElementById(musique.id).appendChild(elem4);
            document.getElementById(musique.id).appendChild(elem5);
            document.getElementById(musique.id).appendChild(elem6);
            document.getElementById(musique.id).appendChild(elem7);

            filtre = null;
        });
    })
    .catch(error => console.error(error));
}
//fonction pour recuperer le json par titre defini, et en faire une liste
function ajaxTitre(){
    let filtre = document.getElementById('filtre').value;
    fetch("http://localhost:8000/musiques/titre/"+filtre, {
        method: 'GET',
        headers: new Headers({
            'Content-Type': 'application/json',
        })
    }).then(response => response.json())
    .then(data => {
        console.log(data);
        console.log(filtre);
        data.forEach(function (musique) {

            let elem0 = document.createElement('a');
            elem0.id = "link"+musique.id;
            elem0.setAttribute('href','#')

            let elem1 = document.createElement('div');
            elem1.className = "divMusique";
            elem1.id = musique.id;
            elem1.setAttribute('onclick','createSelect(this.id);')

            let elem2 = document.createElement('div');
            elem2.className = "divId";
            elem2.innerHTML = "musique n° : " + musique.id;

            let elem3 = document.createElement('img');
            elem3.className = "divImage";
            elem3.setAttribute('src','server/php/files/'+musique.image);

            let elem4 = document.createElement('div');
            elem4.className = "divTitre";
            elem4.innerHTML = "titre : " + musique.titre;

            let elem5 = document.createElement('div');
            elem5.className = "divAuteur";
            elem5.innerHTML = "auteur : " + musique.auteur;

            let elem6 = document.createElement('div');
            elem6.className = "divGenre";
            elem6.innerHTML = "genre : " + musique.genre;

            let elem7 = document.createElement('div');
            elem7.className = "divAnnee";
            elem7.innerHTML = "année : " + musique.annee;

            document.getElementById('musiques').appendChild(elem0);
            document.getElementById('link'+musique.id).appendChild(elem1);
            document.getElementById(musique.id).appendChild(elem2);
            document.getElementById(musique.id).appendChild(elem3);
            document.getElementById(musique.id).appendChild(elem4);
            document.getElementById(musique.id).appendChild(elem5);
            document.getElementById(musique.id).appendChild(elem6);
            document.getElementById(musique.id).appendChild(elem7);

            filtre = null;
        });
    })
    .catch(error => console.error(error));
}
//fonction pour recuperer le json par année defini, et en faire une liste
function ajaxAnnee() {
    let filtre = document.getElementById('filtre').value;
    fetch("http://localhost:8000/musiques/annee/"+filtre, {
        method: 'GET',
        headers: new Headers({
            'Content-Type': 'application/json',
        })
    }).then(response => response.json())
    .then(data => {
        console.log(data);
        console.log(filtre);
        data.forEach(function (musique) {

            let elem0 = document.createElement('a');
            elem0.id = "link"+musique.id;
            elem0.setAttribute('href','#')

            let elem1 = document.createElement('div');
            elem1.className = "divMusique";
            elem1.id = musique.id;
            elem1.setAttribute('onclick','createSelect(this.id);')

            let elem2 = document.createElement('div');
            elem2.className = "divId";
            elem2.innerHTML = "musique n° : " + musique.id;

            let elem3 = document.createElement('img');
            elem3.className = "divImage";
            elem3.setAttribute('src','server/php/files/'+musique.image);

            let elem4 = document.createElement('div');
            elem4.className = "divTitre";
            elem4.innerHTML = "titre : " + musique.titre;

            let elem5 = document.createElement('div');
            elem5.className = "divAuteur";
            elem5.innerHTML = "auteur : " + musique.auteur;

            let elem6 = document.createElement('div');
            elem6.className = "divGenre";
            elem6.innerHTML = "genre : " + musique.genre;

            let elem7 = document.createElement('div');
            elem7.className = "divAnnee";
            elem7.innerHTML = "année : " + musique.annee;

            document.getElementById('musiques').appendChild(elem0);
            document.getElementById('link'+musique.id).appendChild(elem1);
            document.getElementById(musique.id).appendChild(elem2);
            document.getElementById(musique.id).appendChild(elem3);
            document.getElementById(musique.id).appendChild(elem4);
            document.getElementById(musique.id).appendChild(elem5);
            document.getElementById(musique.id).appendChild(elem6);
            document.getElementById(musique.id).appendChild(elem7);

            filtre = null;
        });
    })
    .catch(error => console.error(error));
}
//fonction pour recuperer le json par genre defini, et en faire une liste
function ajaxGenre(){
    let filtre = document.getElementById('filtre').value;
    fetch("http://localhost:8000/musiques/genre/"+filtre, {
        method: 'GET',
        headers: new Headers({
            'Content-Type': 'application/json',
        })
    }).then(response => response.json())
    .then(data => {
        console.log(data);
        console.log(filtre);
        data.forEach(function (musique) {


            let elem0 = document.createElement('a');
            elem0.id = "link"+musique.id;
            elem0.setAttribute('href','#')

            let elem1 = document.createElement('div');
            elem1.className = "divMusique";
            elem1.id = musique.id;
            elem1.setAttribute('onclick','createSelect(this.id);')

            let elem2 = document.createElement('div');
            elem2.className = "divId";
            elem2.innerHTML = "musique n° : " + musique.id;

            let elem3 = document.createElement('img');
            elem3.className = "divImage";
            elem3.setAttribute('src','server/php/files/'+musique.image);

            let elem4 = document.createElement('div');
            elem4.className = "divTitre";
            elem4.innerHTML = "titre : " + musique.titre;

            let elem5 = document.createElement('div');
            elem5.className = "divAuteur";
            elem5.innerHTML = "auteur : " + musique.auteur;

            let elem6 = document.createElement('div');
            elem6.className = "divGenre";
            elem6.innerHTML = "genre : " + musique.genre;

            let elem7 = document.createElement('div');
            elem7.className = "divAnnee";
            elem7.innerHTML = "année : " + musique.annee;

            document.getElementById('musiques').appendChild(elem0);
            document.getElementById('link'+musique.id).appendChild(elem1);
            document.getElementById(musique.id).appendChild(elem2);
            document.getElementById(musique.id).appendChild(elem3);
            document.getElementById(musique.id).appendChild(elem4);
            document.getElementById(musique.id).appendChild(elem5);
            document.getElementById(musique.id).appendChild(elem6);
            document.getElementById(musique.id).appendChild(elem7);

            filtre = null;
        });
    })
    .catch(error => console.error(error));
}
//fonction pour supprimer une musique selon l'id defini en params
function ajaxSupprimer(url) {
    fetch(url, {
        method: 'DELETE',
        headers: new Headers({
            'Authorization': 'Bearer '+auth
        })
    }).then(refresh);
}
//deconnexion de l'utilisateur
function ajaxDeconnexion(url) {
    fetch(url,{
        method: 'GET',
    }).then(refresh)
    .then(response => auth = null)
    .then(response => console.log(auth));
    visibleCrea();
    alert("Vous etes déconnecté");
    console.log("test"+auth);
}
//fonction pour ajouter une musique , revisualisation de la liste de musique par la suite
function ajaxAjouter(url) {
    let jsonArr = {
        titre: document.getElementById('formTitre').value,
        auteur: document.getElementById('formAuteur').value,
        genre: document.getElementById('formGenre').value,
        annee: parseInt(document.getElementById('formAnnee').value),
        image: document.getElementById('formImage').value
    };

    fetch(url, {
        method: 'POST',
        body: JSON.stringify(jsonArr),
        headers: new Headers({
            'Authorization': 'Bearer '+auth,
            'Content-Type': 'application/json'
        })
    }).then(res => res.json())
    .then(response => console.log('Success:', JSON.stringify(response)))
    .catch(error => console.error('Error:', error))
    .then(refresh);
  
}
//fonction pour modifier une musique selon son id en params , revisualisation de la liste de musique par la suite
function ajaxModifier(url) {

    let jsonArr = {
        "id": document.getElementById('formId2').value,
        "auteur": document.getElementById('formAuteur2').value,
        "genre": document.getElementById('formGenre2').value,
        "image": document.getElementById('formImage2').value,
        "annee": document.getElementById('formAnnee2').value,
        "titre": document.getElementById('formTitre2').value
    };

    fetch(url, {
        method: 'PUT',
        body: JSON.stringify(jsonArr),
        headers: new Headers({
            'Authorization': 'Bearer '+auth,
            'Content-Type': 'application/json'
        })
    }).then(refresh);
}
//connexion de l'utilisateur
function ajaxConnexion(url) {
    let user = document.getElementById('email').value;
    let password = document.getElementById('password').value;

    let jsonArr = {
        "username" : user,
        "password" : password
    };

    fetch(url, {
        method: 'POST',
        body: JSON.stringify(jsonArr),
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .then(response => auth = response.token)
    .then(response => console.log(auth));
window.setTimeout (() => { logAlert(); }, 1000);
}
//focntion notifiaction lors de la deconnexion
function logAlert(){
  if((auth === undefined) ^ (auth === null)){
    alert("Erreur login ou mot de passe");
  }else{
    alert("Vous etes connecté");
    visibleCrea();
  }
}

//pour afficher le bouton creation users et admins
function visibleCrea(){
	if (document.getElementById('affichCrea').style.visibility == "hidden")	{
    document.getElementById('affichCrea').style.visibility = "visible";
  }else{
    document.getElementById('affichCrea').style.visibility = "hidden";
  }
}

/* début fonctions pour le panneau de creation users et admins*/
let pop = true;
let countPop = 0;
let pairImpair = true;

function countpopup(){
  countPop = countPop+1;

  if((countPop != 0)&&(countPop%2 == 0)){
      pairImpair = true;
    }else{
      pairImpair = false;
    }
  }

function affich(){
  if((pop == true)){
    blockPop();
    pop = false;
  }else if(pairImpair == true){
      pop = true;
  }
}

function blockPop(){
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
    countpopup();
}

function initialPop(){
    pop = true;
}
/* fin du panneau de creation users et admins*/
//fonction pour ajouter un utilisateur
function ajaxAjoutUser(url) {
let jsonArr = {
    "email" : document.getElementById('emailUser').value,
    "password" : document.getElementById('passwordUser').value,
    "plainPassword"  :  document.getElementById('plainpasswordUser').value
};

    fetch(url, {
        method: 'POST',
        body: JSON.stringify(jsonArr),
        headers: new Headers({
            'Authorization': 'Bearer '+auth,
            'Content-Type': 'application/json'
        })
    }).then(refresh)
    .then(response => console.log('Success:', JSON.stringify(response)))
    .then(refresh)
    .catch(error => console.error('Error:', error));
}
//focntion pour ajouter un administrateur
function ajaxAjoutAdmin(url) {
    let jsonArr = {
        "email" : document.getElementById('emailAdmin').value,
        "password" : document.getElementById('passwordAdmin').value,
        "plainPassword"  :  document.getElementById('plainpasswordAdmin').value
    };

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(jsonArr),
            headers: new Headers({
                'Authorization': 'Bearer '+auth,
                'Content-Type': 'application/json'
            })
        }).then(refresh)
    .then(response => console.log('Success:', JSON.stringify(response)))
    .then(refresh)
    .catch(error => console.error('Error:', error));
    }
//fonction pour afficher la documentaion de l'api
function ajaxDocumentation(){
    fetchJSONFile('./doc.json', function(data){
    // do something with your data
    console.log(data);

    console.log("docu");
        data.forEach(function (documentation) {
        documentation.parameters.forEach(function (param){
            let elem0 = document.createElement('a');
            elem0.id = "link"+documentation.id;
            elem0.setAttribute('href','#');
            elem0.style.cssText = "display:row wrap";

            let elem1 = document.createElement('div');
            elem1.className = "divDocumentation";
            elem1.id = documentation.id;
            elem1.innerHTML = "</br>";
            elem1.style.cssText = "display:inline-block; color:white; margin-left:25px; margin-right:25px; width:400px;";

            let elem2 = document.createElement('div');
            elem2.className = "divPath";
            elem2.innerHTML = "Path : " + documentation.path;
            elem2.style.cssText = "background-color:grey";

            let elem3 = document.createElement('div');
            elem3.className = "divResponse";
            elem3.innerHTML = "Response: " + documentation.response;

            let elem4 = document.createElement('div');
            elem4.className = "divMethod";
            elem4.innerHTML = "Method: " + documentation.method;

            let elem5 = document.createElement('div');
            elem5.className = "divDescription";
            elem5.innerHTML = "Description : " + documentation.description;

            let elem6 = document.createElement('div');
            elem6.className = "divSchema";
            elem6.innerHTML = "Schema: " + documentation.schema;

            let elem7 = document.createElement('div');
            elem7.className = "divName";
            elem7.innerHTML = "Parameters name: " + param.name;

            let elem8 = document.createElement('div');
            elem8.className = "divIN";
            elem8.innerHTML = "Parameters in : " + param.in;

            let elem9 = document.createElement('div');
            elem9.className = "divTYpe";
            elem9.innerHTML = "Parameters type: " + param.type;

            let elem10 = document.createElement('div');
            elem10.className = "divRequired";
            elem10.innerHTML = "Parameters required: " + param.required;

            let elem11 = document.createElement('div');
            elem11.className = "divDescrip";
            elem11.innerHTML = "Parameters description: " + param.description;

    document.getElementById('musiques').appendChild(elem0);
    document.getElementById('link'+documentation.id).appendChild(elem1);
    document.getElementById(documentation.id).appendChild(elem2);
    document.getElementById(documentation.id).appendChild(elem3);
    document.getElementById(documentation.id).appendChild(elem4);
    document.getElementById(documentation.id).appendChild(elem5);
    document.getElementById(documentation.id).appendChild(elem6);
    document.getElementById(documentation.id).appendChild(elem7);
    document.getElementById(documentation.id).appendChild(elem8);
    document.getElementById(documentation.id).appendChild(elem9);
    document.getElementById(documentation.id).appendChild(elem10);
    document.getElementById(documentation.id).appendChild(elem11);
     });
    });
    });
}
