
function clear(){
	let parent1 = document.body;
	let parent2 = document.getElementById('musiques');
	let musSelect = document.getElementById('select');
	while (parent2.firstChild) {
	    parent2.removeChild(parent2.firstChild);
	}
	if (document.getElementById('select') != null) {
		parent1.removeChild(musSelect);
	}
}

function refresh(){
	clear();
	ajaxGet();
}
function ajouter(){
	ajaxAjouter("http://localhost:8000/api/musiques");
}
function connexion(){
	ajaxConnexion("http://localhost:8000/login");
}
function modifier(){
	let id = document.getElementById('formId2').value;
	ajaxModifier("http://localhost:8000/api/musiques/"+id);
}
function parAuteur(){
	clear();
	ajaxAuteur();
}
function parAnnee(){
	clear();
	ajaxAnnee();
}
function parGenre(){
	clear();
	ajaxGenre();
}
function parTitre(){
	clear();
	ajaxTitre();
}
function documentation(){
	clear();
	ajaxDocumentation();
}
function deconnexion(){
	ajaxDeconnexion("http://localhost:8000/logout");
}
function ajouterUser(){
	ajaxAjoutUser("http://localhost:8000/api/inscription-user");
	initialPop();
}
function ajouterAdmin(){
	ajaxAjoutAdmin("http://localhost:8000/api/inscription-admin");
	initialPop();
}


function supprimer(id){
	ajaxSupprimer("http://localhost:8000/api/musiques/"+id);
}

function image(){
	let name = document.getElementById('name').innerHTML;
	document.getElementById('formImage').value = name;
	document.getElementById('formImage2').value = name;
}

function image2(){
	let name = document.getElementById('name').innerHTML;
	document.getElementById('formImage').value = name;
}
