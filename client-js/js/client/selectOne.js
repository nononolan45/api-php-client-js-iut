function createSelect(id){

	if (document.getElementById('select') != null) {
		document.getElementById('select').remove();
	}

	let elem8 = document.createElement('div');
	elem8.id = "select";
	musiques = document.getElementById('musiques');
	document.body.insertBefore(elem8, musiques);


	fetch("http://localhost:8000/musiques/"+id, {
        method: 'GET'
    }).then(response => response.json())
    .then(data => {
        console.log(data);

        let elem2 = document.createElement('div');
		elem2.className = "selectDivId";
		elem2.innerHTML = "musique n° : " + data[0].id;

		let elem3 = document.createElement('div');
		elem3.className = "selectDivAuteur";
		elem3.innerHTML = "auteur : " + data[0].auteur;

		let elem4 = document.createElement('div');
		elem4.className = "selectDivGenre";
		elem4.innerHTML = "genre : " + data[0].genre;

		let elem5 = document.createElement('img');
		elem5.className = "selectDivImage";
		elem5.setAttribute('src','server/php/files/'+data[0].image)

		let elem6 = document.createElement('div');
		elem6.className = "selectDivAnnee";
		elem6.innerHTML = "année : " + data[0].annee;

		let elem7 = document.createElement('div');
		elem7.className = "selectDivTitre";
		elem7.innerHTML = "titre : " + data[0].titre;

		let elem8 = document.createElement('input');
		elem8.className = "supprimer btn btn-success";
		elem8.setAttribute('type','button');
		elem8.setAttribute('name','supprimer');
		elem8.setAttribute('value','supprimer');
		elem8.setAttribute('id',data[0].id);
		elem8.setAttribute('onclick','supprimer(this.id);');

		let elem9 = document.createElement('input');
		elem9.className = "modifier btn btn-success";
		elem9.setAttribute('type','button');
		elem9.setAttribute('name','modifier');
		elem9.setAttribute('value','modifier');
		elem9.setAttribute('id',data[0].id);
		elem9.setAttribute('onclick','ouvrirModifier(this.id);');


		document.getElementById("select").appendChild(elem2);
		document.getElementById("select").appendChild(elem3);
		document.getElementById("select").appendChild(elem4);
		document.getElementById("select").appendChild(elem5);
		document.getElementById("select").appendChild(elem6);
		document.getElementById("select").appendChild(elem7);
		document.getElementById("select").appendChild(elem8);
		document.getElementById("select").appendChild(elem9);
    })
    .catch(error => console.error(error));
}