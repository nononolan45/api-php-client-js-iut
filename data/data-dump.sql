-- MySQL dump 10.16  Distrib 10.1.34-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: musiques
-- ------------------------------------------------------
-- Server version	10.1.34-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `api_musique`
--

DROP TABLE IF EXISTS `api_musique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_musique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auteur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `annee` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_musique`
--

LOCK TABLES `api_musique` WRITE;
/*!40000 ALTER TABLE `api_musique` DISABLE KEYS */;
INSERT INTO `api_musique` VALUES (86,'16 Horsepower','Alternative country, neofolk','220px-Folklore_hp.jpg',2002,'Folklore'),(87,'Superdrag','Rock, Punk','Superdrag-Stereo_360_Sound.jpg',1998,'Stereo 360 Sound'),(88,'Ryan Adams','Alternative country, country','220px-RyanAdamsHeartbreaker.jpg',2000,'Heartbreaker'),(89,'Terry Allen','Country','220px-RyanAdaTerry_Allen_Lubbock%28on_everything%29.jpg',1979,'Lubbock');
/*!40000 ALTER TABLE `api_musique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20181206204456'),('20181206211607'),('20190105173833'),('20190105220856'),('20190105222957'),('20190105223200'),('20190105223315'),('20190105223439'),('20190105223557'),('20190105231049'),('20190105233931'),('20190106105539'),('20190114142335'),('20190115073358'),('20190116185306'),('20190117221631');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usser`
--

DROP TABLE IF EXISTS `usser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7EACCE2BE7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usser`
--

LOCK TABLES `usser` WRITE;
/*!40000 ALTER TABLE `usser` DISABLE KEYS */;
INSERT INTO `usser` VALUES (1,'nolan.parde@sfr.fr','$2y$13$KKLI5z4UI/0V9rpO2Cpwtucj8GxTBoU/bsdxnjRvOnIwd7fPWagJO',1,'a:0:{}'),(3,'admin@admin.root','$2y$13$wU.Rbq.1DjliH1DCAFFpPuG0dE2ZBHNlDgG7PNBPY.IS0e3KG.Yd.',1,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}'),(4,'test@test.test','$2y$13$/ujMzXmwbEV22nVHRx1r6OCJwp8ELOF4ifeXsQU4y5OolmexpvuVW',1,'a:0:{}'),(5,'antoine.antoine@antoine','$2y$13$S.f34COTm10qLW7iL6VlzuCQs3Dbrj6sKlyrzMM1AJ01TMDajh9yG',1,'a:0:{}'),(7,'antoine.antoine.antoine@antoine','$2y$13$eW1NzVrQ/njIF42vdqKTEuvgB1WF3bQkGP9cf4i9t/Orej9rn44se',1,'a:0:{}'),(8,'test.test.test@test','$2y$13$RvYDAQrd2EZhKk3RU/ob2uCkNqidxkRbP9fWNhnVuWyBdXEAyk5i2',1,'a:0:{}'),(9,'xavier.xavier@sfr.fr','$2y$13$vMZLV3s4z8R8xK2y7neBTeLM63Q3gaaO2R4.swQCmaMyYeH4orqXu',1,'a:0:{}'),(10,'test.test@sfr.fr','$2y$13$E2EWExidhrRdh3viAMNgt.TNUjLOHxo8TZWeryJjn/ztMcbru0hbG',1,'a:0:{}'),(11,'test.test@sfr.test','$2y$13$XqrGk/D9BCjMLoglLBcBxO7KPOWBaAAzSCRj1FzOF9TmcRoSlFo1S',1,'a:0:{}'),(19,'nolan.nolan.nolan','nolan',1,'a:0:{}'),(29,'tutu.tutu.tutu','$2y$13$xl.Ou3eTSpm2w7LH02bn5uAlLwlh6I7nmpVfrICuc2aKcq/3eWKDe',1,'a:0:{}'),(30,'admin.admin.admin.admin','$2y$13$S9EuZtDjqhdHcNOw5ZLNfenCUL46ZdDOOBMtG294xWhsqsJqqpMV6',1,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}'),(32,'admin.admin.admin.admin.admin','$2y$13$3bPhyPxgB6TxaCSngE2nQO0Aer2lFllMP/JZGsCXkpO1zoWRiPmWe',1,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}'),(33,'admin.admin.admin.admin.admin.admin','$2y$13$dF0OHz9tAD.ldUc60bb9hOAdporJfrwaugVqSjLWDknU1LwzdCeQa',1,'a:0:{}'),(35,'admin.admin.admin.admin.admin.admin.admin','$2y$13$Gs35NPzAr1BkI.2yByzmmu4tbEqu.debWGKptE9esregNKQH6z/j2',1,'a:0:{}'),(37,'admin.admin.admin.admin.admin.admin.admin.admin.ad','$2y$13$OTN0wKEv.LmxWYWrSV4Y6u.c0PGW9O7fCyQxpAtSG3PPAlP5H3EL.',1,'a:0:{}'),(39,'admin.admin.admin.adidededde,nbjkbjbkjbkjbkbd','$2y$13$7WFnm09h/fIPtH/T7vSxSekCWGzLuWpgJWJ/ltKcT0yFTkJyDRVCK',1,'a:0:{}'),(40,'nolan','$2y$13$DfsVIPVlBnJuRDKtw/fJe.i2quITV5fTDbJGk1Siw0OMsb3LbNZNC',1,'a:0:{}'),(41,'antoine.barbet','$2y$13$fYaN4FGQ2yawdzPU1fxj..0.L58eAWclwbQmg9AJ0lqhZlcGXGiqO',1,'a:0:{}'),(43,'barbet','$2y$13$.scH9l4j7mA9irLRwhXwy.uHzssq51FFpGQpzMRuHi.TmkCIVUYX2',1,'a:0:{}'),(44,'beeeeeeeearbet','$2y$13$CXPtXW4YzENegmeCpMkXButwXb4L4crcVjXV4R4G7mXzaweQHTnUi',1,'a:0:{}'),(45,'barbet.barbet.barbet','$2y$13$vzRZ0TZ3RQUqLQ4QVaMdQ.H/ate6Dw/xHc0vGuffyh5rEhbqMLecK',1,'a:0:{}'),(46,'volvic','$2y$13$od740eIq1NpT8qBWA6H06OvTPwhzVwiaGEhfgDJJ3ZyQI3hyHAqs6',1,'a:0:{}'),(47,'volvic.volvic','$2y$13$dMlSZjpIHaMhApBY/m2PAumU/KNRSSQyCmPX3A0/HV5FqeU/s433i',1,'a:0:{}'),(48,'volvic.volvic.volvic','$2y$13$ygyQa0TN7L2e6pJtFzQ16ueek4DCtYMqxb2rbVfisXE3RMoVejxYi',1,'a:0:{}'),(49,'nono','$2y$13$RFYVyLsGvpT1Sq1z0Rs71eJRNOcqKpJAxzkA5OZ5FENJ3ilyz9/9W',1,'a:0:{}'),(50,'nononono','$2y$13$QPKjHf89kQa7i0efKdy1TuMo4SaA8GFgT725NlMV.3oy/kfPUvhPu',1,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}'),(51,'','$2y$13$AUnRN1zKVowLrzjKX0yn7.KZgecaMFRJ0l6tooShrVwci2Hqnn2iC',1,'a:0:{}'),(52,'ddd@sfr.Fr','$2y$13$m0umvQLV5kDd2fDB4ns0K.ms/../1WeTIrN8yvaJrz4kF1uALSmeS',1,'a:0:{}'),(53,'roza@univ.fr','$2y$13$3eY0IoEjWYb/wAGwq7saPOSBamAJ4hzDGeJ7U6aHxM2pnyzXrimdq',1,'a:0:{}');
/*!40000 ALTER TABLE `usser` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-28 12:29:28
